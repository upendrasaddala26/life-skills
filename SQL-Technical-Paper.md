# SQL
- Create a report on SQL with code samples discussing basics,joins and aggregations.

## What is SQL ?
* SQL stands for Structured Query Language.
* SQL lets you access and manipulate databases.
* SQL became a standard of the American Natinal Standards Institute (ANSI) in 1986, and of the International Organization for Standardization (ISO) in 1987.

## What can SQL do ?
* SQL can execute queries against a database.
* SQL can retrieve data from a database.
* SQL can insert and update records in a database.
* SQL can create and delete records from database.
* SQL can allow to embed within other languages using SQL modules, libraries and pre-compilers.

## SQL Process:
* When you are executing an SQL command for any RDBMS, the system determines the
best way to carry out your request and SQL engine figures out how to interpret the task.
There are various components included in this process.

* These components are 

* 1 Query Dispatcher

* 2.Optimization Engines

* 3.Classic Query Engine

* 4 SQL Query Engine, etc.


## SQL Commands:
 * The standard SQL commands to interact with relational databases are CREATE, SELECT,
INSERT, UPDATE, DELETE and DROP. These commands can be classified into the following
groups based on their nature:

* `CREATE` : Creates a new table, a view of a table, or other object in the database.

* `ALTER` : Modifies an existing database object, such as a table.


* `DROP` : Deletes an entire  or other objects in the
database.


## DML - Data Manipulation Language:

 The Data Manipulation Language (DML) is the subset of SQL used to add, update and delete
data.

* The acronym CRUD refers to all of the major functions that need to be implemented in a
relational database application to consider it complete. Each letter in the acronym can be
mapped to a standard SQL statement:


* `Update`:updates data in database.

* `Delete`:deletes data from database.

* `Read`:extracts data from database.



## SQL JOINS:

  JOIN clause is used to combine rows from two or more tables, based on a related column between them.

* The below iamge explain an overview of SQLJOIN:

* ![Image of SQL join](https://www.codeproject.com/KB/database/Visual_SQL_Joins/Visual_SQL_JOINS_V2.png)
* This is just a simple article visually explaining SQL JOINs. In this article I am going to discuss seven different ways you can return data from two relational tables. The seven Joins I will discuss are: Inner JOIN, Left JOIN, Right JOIN, Outer JOIN, Left Excluding JOIN, Right Excluding JOIN, Outer Excluding JOIN, while providing examples of each.



## Visual Representation of SQL Joins:
* INNER JOIN
* LEFT JOIN
* RIGHT JOIN
* OUTER JOIN
* LEFT JOIN EXCLUDING INNER JOIN
* RIGHT JOIN EXCLUDING INNER JOIN
* OUTER JOIN EXCLUDING INNER JOIN


* Inner Join:
![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/INNER_JOIN.png)

* This is the simplest, most understood Join and is the most common. This query will return all of the records in the left table (table A) that have a matching record in the right table (table B). This Join is written as follows:

SQL:
```
SELECT <select_list> 
FROM Table_A A
INNER JOIN Table_B B
ON A.Key = B.Key

```

* Left Join:
![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/LEFT_JOIN.png)

* This query will return all of the records in the left table (table A) regardless if any of those records have a match in the right table (table B). It will also return any matching records from the right table. This Join is written as follows:

```
SELECT <select_list>
FROM Table_A A
LEFT JOIN Table_B B
ON A.Key = B.Key

```
* Right Join:


    ![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/RIGHT_JOIN.png)

 * This query will return all of the records in the right table (table B) regardless if any of those records have a match in the left table (table A). It will also return any matching records from the left table. This Join is written as follows:

  ```
SELECT <select_list>
FROM Table_A A
RIGHT JOIN Table_B B
ON A.Key = B.Key
```

* Outer Join:


  ![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/FULL_OUTER_JOIN.png)

  * This Join can also be referred to as a FULL OUTER JOIN or a FULL JOIN. This query will return all of the records from both tables, joining records from the left table (table A) that match records from the right table (table B). This Join is written as follows:

```
SELECT <select_list>
FROM Table_A A
FULL OUTER JOIN Table_B B
ON A.Key = B.Key
```

*Left Excluding Join:

   ![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/LEFT_EXCLUDING_JOIN.png)

   * This query will return all of the records in the left table (table A) that do not match any records in the right table (table B). This Join is written as follows:


```
SELECT <select_list> 
FROM Table_A A
LEFT JOIN Table_B B
ON A.Key = B.Key
WHERE B.Key IS NULL
```


 * Right Excluding Join:

   ![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/RIGHT_EXCLUDING_JOIN.png)

* This query will return all of the records in the right table (table B) that do not match any records in the left table (table A). This Join is written as follows:

```
SELECT <select_list>
FROM Table_A A
RIGHT JOIN Table_B B
ON A.Key = B.Key
WHERE A.Key IS NULL
```

* Outer Excluding Join:
 ![image](https://www.codeproject.com/KB/database/Visual_SQL_Joins/OUTER_EXCLUDING_JOIN.png)

 * This query will return all of the records in the left table (table A) and all of the records in the right table (table B) that do not match. I have yet to have a need for using this type of Join, but all of the others, I use quite frequently. This Join is written as follows:

```
SELECT <select_list>
FROM Table_A A
FULL OUTER JOIN Table_B B
ON A.Key = B.Key
WHERE A.Key IS NULL OR B.Key IS NULL
```


## AGGREGATIONS:

 SQL aggregation is the task of collecting a set of values to return a single value. It is done with the help of aggregate functions, such as SUM, COUNT, and AVG. For example, in a database of products, you might want to calculate the average price of the whole inventory.

## How Aggregations Work:

*  SQL COUNT is the most common type of Aggregation in SQL, it counts the number of rows in a column or table. COUNT(*) tells SQL to count the number of rows of the whole table. COUNT(some column) tells SQL to count the number of non-null rows in that column.

*   SELECT COUNT(*)
FROM facebook

* ![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_1.gif)

* SQL goes through all the rows of the table and creates a table with total count of rows.

* ![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_2.png)

* With more complex data, SQL can answer more complex questions such as how many rows are there per state.

![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_3.png)

* The COUNT aggregation can answer this, but SQL needs to be told what to group by in the query. SQL also needs the column it is grouping by in the SELECT statement so that the final table can show the data in its proper groups.


* SELECT State, COUNT(*)
FROM facebook
GROUP BY State

![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_4.gif)

* First SQL will group together data by the column or columns listed in the group by statement in this case state. Then SQL will perform the same COUNT operation as before but check which group to assign the count to.

The data and question can be even more complicated, in this example there is a city column in the table as well. The question could be how many rows are there for each State and City.

Example Table Including City

![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_5.png)

* The COUNT aggregation will get us our answer as long as it is grouped by state and city. Remember SQL needs what we are grouping by in the SELECT statement as well so that these groups will show in the final table.

* SELECT State, City, COUNT(*)
FROM facebook
GROUP BY State, City
![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_6.gif)


## SUM and AVERAGE Aggregation:

* After COUNT, for all other types of aggregation will need you to define the column you are aggregating.

* Wrong:

   SELECT SUM(*)
FROM facebook

* Right:

  SELECT SUM(# of friends)
FROM facebook

*  How would it SUM all the columns in the facebook table? The name column has text and cannot summed. Also there are multiple columns so we cannot SUM the row as a whole like the way we could COUNT the row as a whole. SQL needs to know which column the aggregation should be on, in this case the only numeric column is # of friends.

![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_7.gif)

 The Average aggregation operates similarly to SUM. The data will be more complex for this example.

* SELECT State, AVG(# of friends)
From facebook
GROUP BY State

![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_8.gif)

* To get an average SQL needs the sum of each group and then divide it by the count of rows in each group.

NULLs:
* There are a few scenarios to be aware of when aggregating data that may make you misinterpret the results of your query.

Some cells will not have a value in it. This type of cell is considered null. Null is different than 0 or space “ ”.

The COUNT(*) aggregation will count all rows including Null values. However the COUNT(some column) will count all rows without Null values. Since other aggregations require you specify a column they will exclude Nulls in their calculations.

COUNT(*) count Nulls:
![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_9.gif)

COUNT(# of friends) does not count Nulls:

![image](https://dataschool.com/assets/images/how-to-teach-people-sql/aggregations/aggregations_10.gif)


REFERENCES:
* Data Schools
* Code Project
* Code Academy
* MDN WEB DOCS

