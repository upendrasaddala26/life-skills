# Listening and Active Communication

## 1.Active Listening:
## 1. What are the steps/stratagies to do Active listening ?
* Face the speaker and have eye contact directly with him.
* Pay Attenion and give the speaker individual attention and acknowledge the message.
* Listen without judging or jumping to conclusions.
* Don't start planning what to say next.
* Summarize what you hear and ask questions to check your understanding.
* The listening process involves four stages receiving,understanding,evaluating,and responding.

## 2.Reflective Listening:

## 2.ccording to Fisher's model, what are the key points of Reflective Listening? 
* By enaging in a non-judgemental and empathetic approach,listeners encourage the others to speak freely.
Mirroring the mood of the speaker,reflecting thge emotional state with words and nonverbal communication.This requires the listeners to quiet his mind and focus fully the mood of the speaker.
* Listen more than talk.
* Respond to what is personal in what's being said,rather than to impersonal,distant or abstract material.

## 3.Reflection

## 3.What are the obstacles in your listening process?
* Anxiety can take place from competing personal worries and concerns.
* Self-centeredness. This causes the listener to focus on his or her own thoughts rather than the speaker's words.
* Mental laziness.
* Boredom.
* Sense of superiority.
* Cognitive dissonance. 
* Impatience.


## 4.What can you do to improve your listening?
* Consider eye contact. 
* Be alert, but not intense. 
* Pay attention to nonverbal signs, such as body language and tone. 
* Make a mental image of what the speaker is saying. 
*  Empathise with the speaker. 
*  Provide feedback. 
* Keep an open mind.

## 5.When do you switch to Passive communication style in your day to day life?
* The passive communication style is often used by those who want to come off as indifferent about the topic at hand.
* They either keep their Opinions to themselves or try to make it seem as if they Support every piece of input in the Discussion.
* Passive and Aggressive Communication might work better on some Occasions. 
* If you are Feeling fearful that you are about to be harmed, passive Communication may help to defuse the situation and aggressive Communication might prevent the problem from Getting Worse.

## 6.When do you switch into Aggressive communication styles in your day to day life?
* Aggressiveness is a Mode of Communication and Behavior Where one Expresses their Feelings, Needs, and Rights without regard or respect for the needs, rights, and feelings of others.
HOW To Learn Assertiveness:
* know your values and set boundaries.
* Be honest and open about your feelings.
Train to be active learner.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
* Passive-Aggresive Communication is a style in which individuals appear passive on the surface but are really acting out anger in a subtle, indirect, or behind-the-scenes way.
## How to Stop Your Passive Aggressive Behavior:
* Recognize your behavior.
* Understand why your behavior should be changed.
* Give yourself time.
* Realize it's OK to be angry. 

* Be assertive, not aggressive

## How can you make your communication assertive?
* some Communication assertive examples are:
* Practice saying no. 
* Rehearse what you want to say. 
* Use body language. 
* Keep emotions in check. 
* Start small.
* Don't be guilty.
* Take time.
