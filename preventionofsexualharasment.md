# Prevention Of Sexual Harrasment At WorkPlace:
- The prevention Of sexual Harrasment (PoSH) at work place Act of Indian mandates every organisation to define their sexual harasment policies,prevention systems,procedures and service rules for its employess.
- Recent reports claim a rise in the number of cases registered for sexual harasment at workplaces.


## 1.Three Forms Of Sexual Harasment:
* visual
* verbal
* physical

* 1. Verbal harasment includes :
 making sexual comments about a person's body,making sexual comments or innuendos,asking about sexual fantasies,preferences or history,asking personal questions about someone's social or sex life.

 * 2. Visual harasment is a situation where the individual exposes themselves to another person without victims consent,and the act affects their work performance or attitude.

 * 3.Physical harasment include giving someone a message around the neck or shoulders,touching another person's clothing,hair,or body,hugging,kissing,patting,touching,or rubbing oneself sexually against another person.


 ## THE 2 CATEGORIES OF SEXUAL HARASSMENT:
 * 1.Hostile Work
 * 2.Environment

 ## QUID PRO QUO:
 * It means this for that this happens when an employer or supervisor requesting sexual flavors as a condition for hiring,promotion,advancement,or oppurtunities.
* The defendant and alleged harasser must have made unwanted sexual advances to plaintiff or engaged in other unwanted verbal or physical conduct of a sexual nature.


# What would you do in case you face or witness any incident or repeated incidents of such behaviour?

* Consider an immediate intervention to support someone who is being harassed:

* When you’re informed, you can play a powerful role in stopping and preventing sexual harassment at work. Whether it’s through protection and support of a person who is being harassed, actively discouraging the harasser or simply showing that workplace sexual harassment is not acceptable, we all have a vital role to play.

eveloped by the Hollaback! movement and the bystander program Green
Dot, provide bystanders with a range of options to respond to witnessing harassment
regardless of where it takes place.1
* Direct: If you feel that directly addressing harassment is safe and may be effective,
you can confront the harasser and call out the behavior in the moment. Let them
know you find their behavior inappropriate, intimidating or hostile, and ask them to
stop. This approach may escalate the situation, so consider whether you and the
person being harassed are safe and whether you believe the person being harassed
wants someone to speak up.
* Distract: You can stop an incident by simply interrupting it. Rather than focusing on
the aggressor or action, this subtler intervention allows you to engage the person
being targeted through a distraction – ask a question, start an unrelated
conversation, physically interrupt the incident, or find a reason to call the person out
of that space.
* Delegate: Find an appropriate third party to intervene, such as a supervisor, human
resources officer, security officer or another colleague.
* Delay: If you aren’t able or choose not to intervene in the moment, you can still
support the person who has been harassed by following up with them afterwards.
You can offer acknowledgement and empathy, and can ask whether they need
additional support, resources or documentation of the incident. You can also
confront the harasser later and let them know that you found their behavior
inappropriate.
*  Document: Depending on the circumstances and whether other interventions are
more urgent, it may be most helpful to document what you are witnessing. If you are
able to record an incident or jot down details, be sure to follow up with the targeted
individual and ask them what they would like done with the documentation; do not
share it without their consent

## ACTIONS TO BE TAKEN :

* It's not so much your sexual attraction to someone that can dwindle, but rather, two other types of attraction that begin to decrease first. This then leads to a decrease in sexual attraction.
* If you keep seeing fun pics of them pop up, it might weaken your resistance. It might also make you feel a wave of emotions that you'd rather avoid, which is totally normal. To steer clear of the temptation to scroll through their photos or even show up to the spot where they are, set them to mute on your social media accounts.

* If you want, you can unfriend them or stop following them. But if you’re worried that might invite questions or draw attention to yourself, the mute option is your best bet. You’ll stop seeing their posts but don’t have to stop being friends on social altogether.

* If they are also attracted to you, it can be even harder to not act on your feelings. This can be especially tricky if you can’t totally avoid them. Make it clear with your words and actions that you’re not interested in pursuing things, even if that feels really hard.[4]

*  Resist the urge to flirt even if they initiate it. Even if they start complimenting you and acting interested, resist the urge to reciprocate. If you work with them, say something like, "I’m trying to stay professional. Thanks for respecting that.
*   Avoid physical contact. Don’t casually touch them if you do run into each other. That can send the signal that you’re interested in them.
  It can be super tough to resist these urges, so don’t beat yourself up if you slip. Just try to get yourself back on track with a pep talk.
